package trantor

import (
	"strings"

	log "github.com/cihub/seelog"
	"gitlab.com/trantor/trantor/lib/database"

	"net/http"
)

func loginHandler(h handler) {
	if h.sess.User != "" {
		http.Redirect(h.w, h.r, "/dashboard/", http.StatusFound)
		return
	}

	var data statusData
	data.S = GetStatus(h)
	data.S.Title = "Login -- " + data.S.Title
	h.load("login", data)
}

func loginPostHandler(h handler) {
	user := strings.TrimSpace(h.r.FormValue("user"))
	pass := h.r.FormValue("pass")
	if h.db.ValidPassword(user, pass) {
		log.Info("User ", user, " log in")
		h.sess.LogIn(user)
		h.sess.Notify("Successful login!", "Welcome "+user, "success")
	} else {
		log.Warn("User ", user, " bad user or password")
		h.sess.Notify("Invalid login!", "user or password invalid", "danger")
	}
	h.sess.Save(h.w, h.r)
	http.Redirect(h.w, h.r, h.r.Referer(), http.StatusFound)
}

func createUserHandler(h handler) {
	pass := h.r.FormValue("pass")
	confirmPass := h.r.FormValue("confirmPass")
	if pass != confirmPass {
		h.sess.Notify("Registration error!", "Passwords don't match", "danger")
	} else if pass == "" {
		h.sess.Notify("Registration error!", "The password can't be empty", "danger")
	} else {
		user := strings.TrimSpace(h.r.FormValue("user"))
		err := h.db.AddUser(user, pass)
		if err == nil {
			h.sess.Notify("Account created!", "Welcome "+user+". Now you can login", "success")
		} else {
			h.sess.Notify("Registration error!", err.Error(), "danger")
		}
	}
	h.sess.Save(h.w, h.r)
	http.Redirect(h.w, h.r, h.r.Referer(), http.StatusFound)
}

func dashboardHandler(h handler) {
	if h.sess.User == "" {
		notFound(h)
		return
	}

	lists, err := h.db.GetListsByUser(h.sess.User)
	if err != nil {
		log.Error("Error loading user ", h.sess.User, " lists: ", err)
	}

	var data dashboardData
	data.S = GetStatus(h)
	data.S.Title = "Dashboard -- " + data.S.Title
	data.S.Dasboard = true
	data.Lists = lists
	h.load("dashboard", data)
}

type dashboardData struct {
	S     Status
	Lists []database.BookList
}

func settingsHandler(h handler) {
	if h.sess.User == "" {
		notFound(h)
		return
	}
	if h.r.Method == "POST" {
		current_pass := h.r.FormValue("currpass")
		pass1 := h.r.FormValue("password1")
		pass2 := h.r.FormValue("password2")
		switch {
		case !h.db.ValidPassword(h.sess.User, current_pass):
			h.sess.Notify("Password error!", "The current password given don't match with the user password. Try again", "danger")
		case pass1 != pass2:
			h.sess.Notify("Passwords don't match!", "The new password and the confirmation password don't match. Try again", "danger")
		default:
			err := h.db.SetPassword(h.sess.User, pass1)
			if err != nil {
				log.Warn("Can't update password for user ", h.sess.User, ": ", err)
				h.sess.Notify("Password error!", "An error has ocurred updating the password in the database. Sorry.", "danger")
			} else {
				h.sess.Notify("Password updated!", "Your new password is correctly set.", "success")
			}
		}
		h.sess.Save(h.w, h.r)
	}

	var data statusData
	data.S = GetStatus(h)
	data.S.Title = "Settings -- " + data.S.Title
	h.load("settings", data)
}

func userSubmissionsHandler(h handler) {
	if h.sess.User == "" {
		notFound(h)
		return
	}
	var data submissionsData
	data.S = GetStatus(h)
	data.S.Title = "Submissions"

	var err error
	data.Submissions, err = h.db.GetUserSubmissions(h.sess.User)
	if err != nil {
		log.Warn("Can't get submissions for user ", h.sess.User, ": ", err)
	}

	h.load("submissions", data)
}

type submissionsData struct {
	S           Status
	Submissions []database.Submission
}

func userAdminHandler(h handler) {
	if !h.sess.IsAdmin() {
		notFound(h)
		return
	}

	users, err := h.db.ListUsers()
	if err != nil {
		log.Error("Something went wrong listing users: ", err)
		notFound(h)
		return
	}

	var data userAdminData
	data.S = GetStatus(h)
	data.S.Title = "Users admin -- " + data.S.Title
	data.Users = users
	h.load("user_admin", data)
}

func userAdminPostHandler(h handler) {
	if !h.sess.IsAdmin() {
		notFound(h)
		return
	}

	username := h.r.FormValue("username")
	password := h.r.FormValue("password")
	role := h.r.FormValue("role")
	if password != "" {
		err := h.db.SetPassword(username, password)
		if err != nil {
			h.sess.Notify("An error ocurred!", err.Error(), "danger")
		} else {
			h.sess.Notify("Password updated!", "", "success")
		}
	} else if role != "" {
		err := h.db.SetRole(username, role)
		if err != nil {
			h.sess.Notify("An error ocurred!", err.Error(), "danger")
		} else {
			h.sess.Notify("Role updated!", "", "success")
		}
	}

	userAdminHandler(h)
}

func addUserHandler(h handler) {
	if !h.sess.IsAdmin() {
		notFound(h)
		return
	}

	username := h.r.FormValue("username")
	password := h.r.FormValue("password")
	role := h.r.FormValue("role")
	err := h.db.AddUser(username, password)
	if err != nil {
		h.sess.Notify("An error ocurred!", err.Error(), "danger")
	} else {
		err := h.db.SetRole(username, role)
		if err != nil {
			h.sess.Notify("An error ocurred!", err.Error(), "danger")
		} else {
			h.sess.Notify("User created!", "", "success")
		}
	}

	userAdminHandler(h)
}

type userAdminData struct {
	S     Status
	Users []database.User
}
