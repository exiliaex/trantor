// +build noprometheus

package instrument

type dummyInst struct{}

func Init() Instrument {
	return &dummyInst{}
}

func (i dummyInst) Request(req RequestData) {}
