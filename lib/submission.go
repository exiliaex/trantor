package trantor

import (
	"net/http"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
	"github.com/prometheus/common/log"
	"gitlab.com/trantor/trantor/lib/database"
)

const (
	newItemsPage = 5
)

func submissionHandler(h handler) {
	var data submissionData
	var err error

	submissionID := mux.Vars(h.r)["submissionID"]
	data.SubmissionID = submissionID
	data.Submissions, err = h.db.GetSubmission(submissionID)
	if err != nil {
		log.Error("Can get submission ", submissionID, ": ", err)
	}
	data.S = GetStatus(h)
	data.S.Title = "Submission -- " + data.S.Title
	h.load("submission", data)
}

type submissionData struct {
	S            Status
	SubmissionID string
	Submissions  []database.Submission
}

func submissionCommentHandler(h handler) {
	submissionID := mux.Vars(h.r)["submissionID"]
	bookID := mux.Vars(h.r)["id"]
	comment := h.r.FormValue("comment")

	err := h.db.UpdateSubmissionComment(submissionID, bookID, comment)
	if err != nil {
		log.Error("Adding comment (submission: ", submissionID, ", book: ", bookID, ") <", comment, ">: ", err)
		h.sess.Notify("Error adding a comment!", "Can't add the comment rigt now. Try again later or report it to the site admins", "danger")
	} else {
		h.sess.Notify("Comment added!", "", "success")
	}
	http.Redirect(h.w, h.r, "/submission/"+submissionID, http.StatusFound)
}

type submissionBook struct {
	TitleFound  int
	AuthorFound int
	Comment     string
	B           database.Book
}
type mSubmissionData struct {
	S      Status
	Found  int
	Books  []submissionBook
	Page   int
	Next   string
	Prev   string
	Search string
}

func moderateSubmissionsHandler(h handler) {
	if !h.sess.IsModerator() {
		notFound(h)
		return
	}

	err := h.r.ParseForm()
	if err != nil {
		http.Error(h.w, err.Error(), http.StatusInternalServerError)
		return
	}
	req := strings.Join(h.r.Form["q"], " ")
	page := 0
	if len(h.r.Form["p"]) != 0 {
		page, err = strconv.Atoi(h.r.Form["p"][0])
		if err != nil {
			page = 0
		}
	}
	res, num, _ := h.db.GetNewBooks(req, newItemsPage, page*newItemsPage)

	var data mSubmissionData
	data.S = GetStatus(h)
	data.S.Title = "New books -- " + data.S.Title
	data.Found = num
	if num-newItemsPage*page < newItemsPage {
		data.Books = make([]submissionBook, num-newItemsPage*page)
	} else {
		data.Books = make([]submissionBook, newItemsPage)
	}
	for i, b := range res {
		data.Books[i].B = b
		// FIXME: there is a heavy performance issue searching for title/authors
		//_, data.Books[i].TitleFound, _ = h.db.GetBooks("title:"+b.Title, 1, 0)
		//_, data.Books[i].AuthorFound, _ = h.db.GetBooks("author:"+strings.Join(b.Authors, " author:"), 1, 0)
		data.Books[i].Comment, err = h.db.GetComment(b.ID)
		if err != nil {
			log.Error("Error getting comment for ", b.Title, " (", b.ID, "): ", err)
		}
	}
	data.Page = page + 1
	if num > (page+1)*newItemsPage {
		data.Next = "/submission/moderate/?q=" + req + "&p=" + strconv.Itoa(page+1)
	}
	if page > 0 {
		data.Prev = "/submission/moderate/?q=" + req + "&p=" + strconv.Itoa(page-1)
	}
	data.Search = req
	h.load("submission_moderate", data)
}

func storeHandler(h handler) {
	if !h.sess.IsModerator() {
		notFound(h)
		return
	}

	var titles []string
	ids := strings.Split(mux.Vars(h.r)["ids"], "/")
	for _, id := range ids {
		if id == "" {
			continue
		}
		book, err := h.db.GetBookID(id)
		if err != nil {
			h.sess.Notify("Book not found!", "The book with id '"+id+"' is not there", "danger")
			continue
		}
		if err != nil {
			h.sess.Notify("An error ocurred!", err.Error(), "danger")
			log.Error("Error getting book for storing '", book.Title, "': ", err.Error())
			continue
		}
		err = h.db.ActiveBook(id)
		if err != nil {
			h.sess.Notify("An error ocurred!", err.Error(), "danger")
			log.Error("Error storing book '", book.Title, "': ", err.Error())
			continue
		}

		err = h.db.UpdateSubmissionByBook(id, "Included in the library", &book)
		if err != nil {
			log.Error("There was a problem updating the submission: ", err)
		}

		titles = append(titles, book.Title)
	}
	if titles != nil {
		h.sess.Notify("Store books!", "The books '"+strings.Join(titles, ", ")+"' are stored for public download", "success")
	}
	h.sess.Save(h.w, h.r)
	http.Redirect(h.w, h.r, h.r.Referer(), http.StatusFound)
}
