package storage

import (
	"io"
	"os"
)

type Store interface {
	Create(id string, name string) (io.WriteCloser, error)
	Store(id string, file io.Reader, name string) (size int64, err error)
	Get(id string, name string) (File, error)
	Delete(id string) error
}

type File interface {
	io.ReadCloser
	io.ReaderAt
	io.Seeker
	Stat() (fi os.FileInfo, err error)
}

func Init(path string) (Store, error) {
	st := new(fsStore)
	st.path = path

	_, err := os.Stat(path)
	if err != nil {
		err = mkstore(st.path)
	}
	return st, err
}

func RO(st Store) Store {
	return &roStore{st}
}
