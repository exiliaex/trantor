package trantor

import (
	"net/http"
	"strings"

	"gitlab.com/trantor/trantor/lib/database"

	log "github.com/cihub/seelog"
	"github.com/gorilla/mux"
)

func listHandler(h handler) {
	listID := mux.Vars(h.r)["listID"]
	list, err := h.db.GetBookList(listID)
	if err != nil {
		log.Error("Error loading list ", listID, ": ", err)
		h.sess.Notify("Something went wrong!", "Could not load the list", "danger")
		http.Redirect(h.w, h.r, h.r.Referer(), http.StatusFound)
		return
	}
	if list == nil {
		notFound(h)
		return
	}

	var data listData
	data.S = GetStatus(h)
	data.S.Title = list.Title + " -- " + data.S.Title
	data.List = list

	action := mux.Vars(h.r)["action"]
	if action == "edit" && h.sess.User == list.User.Username {
		h.load("list_edit", data)
	} else {
		h.load("list", data)
	}
}

type listData struct {
	S    Status
	List *database.BookList
}

func listPostHandler(h handler) {
	if h.sess.User == "" {
		notFound(h)
		return
	}

	listTitle := h.r.FormValue("list")
	userLists, err := h.db.GetListsByUser(h.sess.User)
	if err != nil {
		log.Error("Error loading user (", h.sess.User, ") lists: ", err)
		h.sess.Notify("Something went wrong!", "Could not add book to the list", "danger")
		http.Redirect(h.w, h.r, h.r.Referer(), http.StatusFound)
		return
	}

	var list *database.BookList
	for _, l := range userLists {
		if strings.ToLower(l.Title) == strings.ToLower(listTitle) {
			list = &l
			break
		}
	}

	var listID string
	if list == nil {
		listID = GenID()
		err = h.db.NewBookList(listID, listTitle, h.sess.User, []string{})
		if err != nil {
			log.Error("Error creating list ", listTitle, " by user ", h.sess.User, ": ", err)
			h.sess.Notify("Something went wrong!", "Could not add book to the list", "danger")
			http.Redirect(h.w, h.r, h.r.Referer(), http.StatusFound)
			return
		}
	} else {
		listID = list.ListID
	}

	bookID := h.r.FormValue("book_id")
	err = h.db.AddBookToList(listID, bookID)
	if err != nil {
		log.Error("Error adding book ", bookID, " to list ", listTitle, "(", listID, "): ", err)
		h.sess.Notify("Something went wrong!", "Could not add book to the list", "danger")
		http.Redirect(h.w, h.r, h.r.Referer(), http.StatusFound)
		return
	}

	http.Redirect(h.w, h.r, "/list/"+listID, http.StatusFound)
}

func listEditPostHandler(h handler) {
	listID := mux.Vars(h.r)["listID"]
	list, err := h.db.GetBookList(listID)
	if err != nil || list == nil {
		log.Error("Error loading list ", listID, ": ", err)
		h.sess.Notify("Something went wrong!", "Could not save the list", "danger")
		http.Redirect(h.w, h.r, h.r.Referer(), http.StatusFound)
		return
	}
	if h.sess.User != list.User.Username {
		h.sess.Notify("You are not the owner of the list!", "You can't edit it", "danger")
		http.Redirect(h.w, h.r, h.r.Referer(), http.StatusFound)
		return
	}

	title := h.r.FormValue("title")
	description := strings.Split(h.r.FormValue("description"), "\n")
	err = h.db.UpdateBookList(listID, title, description)
	if err != nil {
		log.Error("Error editing list ", list.Title, "(", listID, "): ", err)
		h.sess.Notify("Something went wrong!", "Could not edit the list", "danger")
		http.Redirect(h.w, h.r, h.r.Referer(), http.StatusFound)
		return
	}

	h.sess.Notify("List updated!", "", "success")
	http.Redirect(h.w, h.r, "/list/"+listID, http.StatusFound)
}

func listRemoveHandler(h handler) {
	listID := mux.Vars(h.r)["listID"]
	bookID := mux.Vars(h.r)["bookID"]
	list, err := h.db.GetBookList(listID)
	if err != nil || list == nil {
		log.Error("Error loading list ", listID, ": ", err)
		h.sess.Notify("Something went wrong!", "Could not remove books from the list", "danger")
		http.Redirect(h.w, h.r, h.r.Referer(), http.StatusFound)
		return
	}
	if h.sess.User != list.User.Username {
		h.sess.Notify("You are not the owner of the list!", "You can't remove books from it", "danger")
		http.Redirect(h.w, h.r, h.r.Referer(), http.StatusFound)
		return
	}

	err = h.db.DeleteBookFromList(listID, bookID)
	if err != nil {
		log.Error("Error remove book ", bookID, " from list ", listID, "): ", err)
		h.sess.Notify("Something went wrong!", "Could not remove book", "danger")
		http.Redirect(h.w, h.r, h.r.Referer(), http.StatusFound)
		return
	}

	http.Redirect(h.w, h.r, "/list/"+listID+"/edit", http.StatusFound)
}
